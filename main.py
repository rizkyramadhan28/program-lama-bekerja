# Buatlah program untuk menentukan durasi kerja pegawai.
# Jam masuk dan jam keluar harus diinputkan.
# Pegawai harus bekerja kurang dari 12 jam.

print("=====================================")
print("\tProgram Lama Bekerja")
print("=====================================\n")

# Menginputkan jam masuk

jam_masuk = int(input("Masukkan Jam Masuk\t: "))

# Selama jam masuk yang diinputkan kurang dari 1 atau lebih dari 12
# maka jam masuk harus terus diinputkan sampai benar.

while (jam_masuk < 1 or jam_masuk > 12):
    jam_masuk = int(input("Masukkan Jam Masuk\t: "))

# Menginputkan jam keluar

jam_keluar = int(input("Masukkan Jam Keluar\t: "))

# Selama jam keluar yang diinputkan kurang dari 1 atau lebih dari 12
# maka jam keluar harus terus diinputkan sampai benar.

while (jam_keluar < 1 or jam_keluar > 12):
    jam_keluar = int(input("Masukkan Jam Keluar\t: "))

# Jika jam keluar lebih dari jam masuk maka langsung saja kurangi.
# Selain itu, jam masuk harus dikurangi 12 dan ditambah jam keluar
# agar hitungan durasi bekerja menjadi benar.

if (jam_keluar >= jam_masuk):
    lama_kerja = jam_keluar - jam_masuk
else:
    lama_kerja = (12 - jam_masuk) + jam_keluar

# Tampilkan durasi bekerja.

if (lama_kerja > 0):
    print(f"\nLama Bekerja\t\t: {lama_kerja} Jam")
else:
    print(f"\nPegawai tidak boleh bekerja lebih dari 12 jam!")
